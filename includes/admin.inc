<?php

/**
 * @file
 * Admin functions.
 */

/**
 * Installs the missing tables from the schema.
 */
function handy_install_schema($module) {
  module_load_include('install', $module);
  $schema = drupal_get_schema_unprocessed($module);
  _drupal_schema_initialize($schema, $module, FALSE);
  foreach ($schema as $name => $table) {
    if (!db_table_exists($table['name'])) {
      db_create_table($name, $table);
    }
  }
}

/**
 * Checks if the page is admin.
 *
 * @return bool
 */
function current_path_is_admin() {
  return path_is_admin(current_path());
}

/**
 * Returns TRUE if running page from cli (drush) or on an admin page.
 *
 * @return bool
 */
function cli_or_admin() {
  return ('cli' == php_sapi_name()) || current_path_is_admin();
}

/**
 * Returns the handy list of options for the time select list.
 *
 * @param string $off_label
 * @param int $min
 * @param int $max
 *
 * @return array
 */
function handy_time_options($off_label = '- none -', $min = 0, $max = 0) {
  $options = array();

  $time_base = 60;
  $time_base_limits = array(
    array(60, 'minute'),
    array(24, 'hour'),
    array(30, 'day'),
    array(13, 'month'),
  );
  foreach ($time_base_limits as $i => $time_base_limit) {
    list($time_base_limit, $time_base_limit_name) = $time_base_limit;
    if ($i) {
      $time_base *= $time_base_limits[$i - 1][0];
    }
    for ($j = 1; $j < $time_base_limit; $j++) {
      if ($time_base * $j >= $min) {
        if ($max && ($time_base * $j > $max)) {
          break 2;
        }
        $options[$time_base * $j] = $j . ' ' . $time_base_limit_name . ($j == 1 ? '' : 's');
      }
    }
  }

  $options[0] = $off_label;

  return $options;
}

/**
 * Simple email sending with mimemail.
 *
 * @param $to
 * @param string $subject
 * @param string $body
 * @param string $from
 *
 * @return array
 */
function handy_mimemail($to, $subject = '', $body = '', $from = NULL) {
  // Module should exist.
  if (!module_exists('mimemail')) {
    return FALSE;
  }

  $params = array(
      'context' => array(
        'subject' => $subject,
        'body' => $body,
      ),
    ) + array(
      'plaintext' => '',
      'attachments' => '',
    );
  return drupal_mail('mimemail', $key = uniqid('handy_mimemail_'), $to, $language = LANGUAGE_NONE, $params, $from);
}

/**
 *
 * @param     $function_name
 * @param int $count
 *
 * @return bool
 */
function function_call_limit($function_name, $count = 1) {
  static $once = array();

  if (!isset($once[$function_name])) {
    $once[$function_name] = 0;
  }
  $once[$function_name]++;

  return $once[$function_name] > $count;
}

/**
 *
 * @param $function_name
 *
 * @return bool
 */
function function_call_limit_once($function_name) {
  return function_call_limit($function_name);
}

/**
 * Sets the module weight.
 *
 * @param      $module_name
 * @param null $weight
 *  TRUE to make the weight the minimal possible.
 */
function handy_module_set_weight($module_name, $weight = NULL) {
  if (is_null($weight)) {
    $weight = TRUE;
  }

  // min possible
  if (TRUE === $weight) {
    $weight = db_query("SELECT MIN(weight) FROM {system}")->fetchField();
    if ($weight > -65535) {
      $weight = -65535;
    }
    else {
      $weight--;
    }
  }

  // numeric
  if (!is_numeric($weight)) {
    $weight = 0;
  }

  db_update('system')->fields(array('weight' => $weight))->condition('name', $module_name)->execute();
}
