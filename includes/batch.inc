<?php

/**
 * @file
 * Debug functions.
 */

/**
 * @param array $operations
 */
function handy_batch_set($operations = array()) {
  $batch = array(
    'operations' => $operations,
    'finished' => 'handy_batch_finished',
  );

  batch_set($batch);
}

/**
 * @param $success
 * @param $results
 * @param $operations
 */
function handy_batch_finished($success, $results, $operations) {
  if ($success) {
    drupal_set_message(t('@count items processed.', array('@count' => count($results))));
  }
  else {
    $error_operation = reset($operations);
    drupal_set_message(t('An error occurred while processing @operation with arguments : @args', array(
      '@operation' => $error_operation[0],
      '@args' => print_r($error_operation[0], TRUE),
    )));
  }
}
