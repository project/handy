<?php 

/**
 * @file
 * User functions.
 */

/**
 * @param $role
 * @param null $account
 *
 * @return bool
 */
function user_has_role_by_name($role, $account = NULL) {
  if (is_null($account)) {
    global $user;
    $account = $user;
  }

  return (bool) in_array($role, $account->roles);
}

/**
 * @param $role
 * @param $account
 * @param bool $flag
 */
function user_add_role_by_name($role, $account, $flag = TRUE) {
  $roles = user_roles();
  if (FALSE !== ($role_id = array_search($role, $roles))) {
    if ($flag) {
      $account->roles[$role_id] = $role;
    }
    else {
      unset($account->roles[$role_id]);
    }
    user_save($account, array('roles' => $account->roles));
  }
}
