<?php

/**
 * @file
 * DS functions.
 */

/**
 * Defines the shortcut function for hook_ds_field_info().
 *
 * See an example below.
 * 
 * @param $field_name
 * @param $field_label
 * @param array $ui_limit
 * @param string $function_name_prefix
 *
 * @return array
 */
function handy_ds_field_info($field_name, $field_label, $ui_limit = array(), $function_name_prefix = '') {
  return array(
    'title' => t('DS Field: ' . $field_label),
    'field_type' => DS_FIELD_TYPE_FUNCTION,
    'function' => 'handy_ds_field_render',
    'properties' => array(
      'field_name' => $field_name,
      'function_name_prefix' => $function_name_prefix,
    ),
    'ui_limit' => !empty($ui_limit) ? (is_array($ui_limit) ? $ui_limit : array($ui_limit)) : array(),
  );
}

/**
 * Rendering function for hook_ds_field_info().
 *
 * @param $field
 *
 * @return string
 */
function handy_ds_field_render($field) {
  return _handy_ds_field_render($field['properties']['field_name'], $field['properties']['function_name_prefix'], $field['entity']);
}

/**
 * Rendering helper function for hook_ds_field_info().
 *
 * @param $field_name
 * @param $function_name_prefix
 * @param null $entity
 *
 * @return string
 */
function _handy_ds_field_render($field_name, $function_name_prefix, $entity = NULL) {
  $content = '';

  if ($entity) {
    $function = $function_name_prefix . '__' . $field_name;
    if (function_exists($function)) {
      $content = $function($entity);
    }
  }

  return $content;
}

/**
 * Implements hook_ds_fields_info().
 *
function MODULE_ds_fields_info($entity_type) {
  $fields = array();

  if ('node' == $entity_type) {
    // Will build the field with:
    // Name "DS Field: Field Label"
    // Machine Name "custom_field_name"
    // Rendering Function "custom_field_render__field_name($node)"
    $fields[$entity_type]['custom_field_name'] = handy_ds_field_info('field_name', 'Field Label', '*|*', 'custom_field_render');
  }

  return $fields;
}
 */
