<?php

/**
 * @file
 * Theme functions.
 */

/**
 * To prevent Drupal's Theme API from throwing notices
 *  for template-based theme callbacks
 *  with preprocess functions used.
 *
 * @param $variables
 */
function theme_preprocess_theme_attributes(&$variables) {
  foreach (array(
             'classes_array',
             'attributes_array',
             'title_attributes_array',
             'content_attributes_array',
           ) as $key) {
    if (!isset($variables[$key])) {
      $variables[$key] = array();
    }
  }
}
