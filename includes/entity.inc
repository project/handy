<?php

/**
 * @file
 * Entity functions.
 */

/**
 * Get the current node.
 *
 * @param array $types
 *
 * @return bool|stdClass
 */
function menu_get_node($types = array()) {
  if ($node = menu_get_object()) {
    if (!empty($types)) {
      if (!is_array($types)) {
        $types = array($types);
      }
      if (!in_array($node->type, $types)) {
        return FALSE;
      }
    }
    return $node;
  }
  return FALSE;
}

/**
 * Get the current term.
 *
 * @param array $vocabulary_machine_names
 *
 * @return bool|stdClass
 */
function menu_get_term($vocabulary_machine_names = array()) {
  if ($term = menu_get_object('taxonomy_term', 2)) {
    if (!empty($vocabulary_machine_names)) {
      if (!is_array($vocabulary_machine_names)) {
        $vocabulary_machine_names = array($vocabulary_machine_names);
      }
      if (!in_array($term->vocabulary_machine_name, $vocabulary_machine_names)) {
        return FALSE;
      }
    }
    return $term;
  }
  return FALSE;
}

/**
 * @return array|bool
 */
function menu_get_entity($position = NULL) {
  $router_item = menu_get_item();
  if (!empty($router_item['load_functions'])) {
    if (is_null($position)) {
      $load_function = reset($router_item['load_functions']);
      $position = key($router_item['load_functions']);
    }
    elseif (isset($router_item['load_functions'][$position])) {
      $load_function = $router_item['load_functions'][$position];
    }
    else {
      return FALSE;
    }
    $suffix = '_load';
    if ($suffix === substr($load_function, -strlen($suffix))) {
      $entity_type = substr($load_function, 0, -strlen($suffix));
      $entity = $router_item['map'][$position];
      return array($entity_type, $entity);
    }
  }
  return FALSE;
}

/**
 *
 * @param $term_name
 * @param $vocabulary_machine_name
 * @param bool $create
 *
 * @return bool|mixed|\stdClass
 */
function taxonomy_term_find($term_name, $vocabulary_machine_name, $create = TRUE) {
  // Validation.
  $length = strlen($term_name);
  if (!$length || ($length > 255)) {
    return FALSE;
  }

  if ($terms = taxonomy_get_term_by_name($term_name, $vocabulary_machine_name)) {
    return reset($terms);
  }
  elseif ($create) {
    $vocabulary = taxonomy_vocabulary_machine_name_load($vocabulary_machine_name);

    $term = new stdClass();
    $term->name = $term_name;
    $term->vid = $vocabulary->vid;
    taxonomy_term_save($term);

    return $term;
  }

  return FALSE;
}

/**
 * @param $term_name
 * @param $vocabulary_machine_name
 * @param $id
 * @param bool $create
 * @param string $field_name
 *
 * @return bool|mixed|null|\stdClass
 */
function taxonomy_term_find_by_id($term_name, $vocabulary_machine_name, $id, $create = TRUE, $field_name = 'field_id') {
  $term = NULL;

  // Upsert.
  $query = new EntityFieldQuery();
  $query
    ->entityCondition('entity_type', 'taxonomy_term')
    ->entityCondition('bundle', $vocabulary_machine_name)
    ->fieldCondition($field_name, 'value', $id, '=');
  if ($result = $query->execute()) {
    $taxonomy_terms = $result['taxonomy_term'];
    if ($taxonomy_terms) {
      $term = reset($taxonomy_terms);
      $term = taxonomy_term_load($term->tid);
    }
  }

  if (!$term && $create) {
    $vocabulary = taxonomy_vocabulary_machine_name_load($vocabulary_machine_name);

    $term = new stdClass();
    $term->name = $term_name;
    $term->vid = $vocabulary->vid;
    $term->{$field_name}[LANGUAGE_NONE] = array(array('value' => $id));

    taxonomy_term_save($term);
  }

  return $term;
}

/**
 * Loads the keyed array of the terms.
 *
 * @param $vocabulary_name
 * @param int $parent
 * @param null $max_depth
 * @param bool $load_entities
 *
 * @return array
 */
function handy_taxonomy_get_tree($vocabulary_name, $parent = 0, $max_depth = NULL, $load_entities = FALSE) {
  $output = array();
  if ($vocabulary = taxonomy_vocabulary_machine_name_load($vocabulary_name)) {
    $tree = taxonomy_get_tree($vocabulary->vid, $parent, $max_depth, $load_entities);
    foreach ($tree as $term) {
      $output[$term->tid] = $term;
    }
  }

  return $output;
}

/**
 * Loads the keyed array of the terms or their tids.
 *
 * @param $vocabulary_name
 * @param int $parent
 * @param null $max_depth
 * @param bool $load_entities
 *
 * @return array
 */
function handy_taxonomy_get_tids($vocabulary_name, $parent = 0, $max_depth = NULL, $load_entities = FALSE) {
  return array_keys(handy_taxonomy_get_tree($vocabulary_name, $parent, $max_depth, $load_entities));
}
