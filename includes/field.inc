<?php

/**
 * @file
 * Field functions.
 */

/**
 * @param $entity
 * @param $field_name
 * @param null $langcode
 *
 * @return bool
 */
function field_get_items_entity($entity, $field_name, $langcode = NULL) {
  if (is_null($langcode)) {
    $langcode = LANGUAGE_NONE;
  }
  return isset($entity->{$field_name}[$langcode]) ? $entity->{$field_name}[$langcode] : FALSE;
}

/**
 * @param $entity
 * @param $field_name
 * @param null $index
 * @param null $key
 * @param string $entity_type
 * @param null $langcode
 *
 * @return bool|null
 */
function field_get_item($entity, $field_name, $index = NULL, $key = NULL, $entity_type = 'node', $langcode = NULL) {
  // items
  $items = ('node' == $entity_type) ? field_get_items($entity_type, $entity, $field_name, $langcode) : field_get_items_entity($entity, $field_name, $langcode);
  if (!$items) {
    return NULL;
  }

  // no index specified
  if (is_null($index)) {
    return $items;
  }

  // no index found
  if (!isset($items[$index])) {
    return NULL;
  }

  // no key specified
  if (is_null($key)) {
    return $items[$index];
  }

  // no key found
  if (!isset($items[$index][$key])) {
    return NULL;
  }

  // key found
  return $items[$index][$key];
}

/**
 * @param $entity
 * @param $field_name
 * @param null $index
 * @param null $key
 * @param string $entity_type
 * @param null $langcode
 *
 * @return array
 */
function field_get_values($entity, $field_name, $key, $entity_type = 'node', $langcode = NULL) {
  $values = array();

  if ($items = field_get_item($entity, $field_name, NULL, NULL, $entity_type, $langcode)) {
    foreach ($items as $item) {
      if (isset($item[$key])) {
        $values[] = $item[$key];
      }
    }
  }

  return $values;
}
