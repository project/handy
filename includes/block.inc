<?php

/**
 * @file
 * Block functions.
 */

/**
 * @param $module
 * @param $delta
 *
 * @return mixed
 */
function block_render($module, $delta) {
  $block = block_load($module, $delta);
  foreach (array('region', 'title') as $key) {
    $block->{$key} = '';
  }
  $block = _block_render_blocks(array($block));
  $block = _block_get_renderable_array($block);
  return render($block);
}
