<?php 

/**
 * @file
 * String functions.
 */

/**
 * trims text to a maximum length, splitting at last word break, and
 * (optionally) appending ellipses and stripping HTML tags
 *
 * @param string $input text to trim
 * @param int $length maximum number of characters allowed
 * @param bool $ellipses if ellipses (...) are to be added
 * @param bool $strip_html if html tags are to be stripped
 *
 * @return string
 *
 * @see https://gist.github.com/phylsys/fbda68b8e78803dca813
 */
function trim_better($input, $length, $ellipses = TRUE, $strip_html = TRUE) {
  //strip tags, if desired
  if ($strip_html) {
    $input = strip_tags($input);
  }

  //strip leading and trailing whitespace
  $input = trim($input);

  //no need to trim, already shorter than trim length
  if (strlen($input) <= $length) {
    return $input;
  }

  //leave space for the ellipses (...)
  if ($ellipses) {
    $length -= 3;
  }

  //this would be dumb, but I've seen dumber
  if ($length <= 0) {
    return '';
  }

  //find last space within length
  //(add 1 to length to allow space after last character - it may be your lucky day)
  $last_space = strrpos(substr($input, 0, $length + 1), ' ');
  if ($last_space === FALSE) {
    //lame, no spaces - fallback to pure substring
    $trimmed_text = substr($input, 0, $length);
  }
  else {
    //found last space, trim to it
    $trimmed_text = substr($input, 0, $last_space);
  }

  //add ellipses (...)
  if ($ellipses) {
    $trimmed_text .= '...';
  }

  return $trimmed_text;
}

/**
 *
 * @param $text
 *
 * @return mixed|string
 */
function handy_machine_name($text, $replacement = '-') {
  $text = drupal_strtolower($text);
  $text = preg_replace('@[^a-z0-9]+@', $replacement, $text);
  $text = trim($text, $replacement);

  return $text;
}

/**
 *
 * @param $text
 *
 * @return string
 */
function handy_check_plain($text) {
  return check_plain(preg_replace('/[^(\x20-\x7F)]*/', '', html_entity_decode($text, ENT_QUOTES)));
}

/**
 * Remove 4-byte UTF8 symbols.
 *
 * @param string $string
 * @param string $replacement
 *
 * @return string
 *
 * @see https://www.drupal.org/project/strip_utf8mb4
 * 
 */
function strip_utf8mb4($string, $replacement = '', &$replacements_done = array()) {
  // Strip overly long 2 byte sequences, as well as characters
  //  above U+10000 and replace with $replace_text
  $processed_text_data = preg_replace('/[\x00-\x08\x10\x0B\x0C\x0E-\x19\x7F]' .
    '|[\x00-\x7F][\x80-\xBF]+' .
    '|([\xC0\xC1]|[\xF0-\xFF])[\x80-\xBF]*' .
    '|[\xC2-\xDF]((?![\x80-\xBF])|[\x80-\xBF]{2,})' .
    '|[\xE0-\xEF](([\x80-\xBF](?![\x80-\xBF]))|(?![\x80-\xBF]{2})|[\x80-\xBF]{3,})/S',
    $replacement, $string, -1, $replacements_done[]);

  // Strip overly long 3 byte sequences and UTF-16 surrogates and replace with $replace_text
  $processed_text_data = preg_replace('/\xE0[\x80-\x9F][\x80-\xBF]' .
    '|\xED[\xA0-\xBF][\x80-\xBF]/S', $replacement, $processed_text_data, -1, $replacements_done[]);

  return $processed_text_data;
}
