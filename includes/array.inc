<?php

/**
 * @file
 * Array functions.
 */

/**
 * Filters the array with list of allowed keys
 *
 * @param array $array
 * @param array $allowed_keys
 *
 * @return array
 */
function array_allowed_keys(array $array, $allowed_keys = array()) {
  $allowed_keys = array_unique($allowed_keys);
  return array_intersect_key($array, array_flip($allowed_keys));
}

/**
 * Recursively ksort-ing the array.
 *
 * @param array $array
 *
 * @return bool
 */
function ksort_recursive(array &$array) {
  foreach ($array as & $value) {
    if (is_array($value)) {
      ksort_recursive($value);
    }
  }
  return ksort($array);
}

/**
 * Rerurns hashed string for the given object.
 *
 * @param array $array
 * @param string $prefix
 *
 * @return string
 */
function handy_array_hash($array = array(), $prefix = '') {
  ksort_recursive($array);
  return $prefix . sha1(serialize($array));
}

/**
 * Converts DOMNode to an array.
 *
 * @param \DOMNode $dom
 * @param int $depth
 *
 * @return array|string
 */
function xml_to_array(DOMNode $dom, $depth = -1) {
  $result = array();

  if ($dom->hasAttributes()) {
    $attrs = $dom->attributes;
    foreach ($attrs as $attr) {
      $result['@attributes'][$attr->name] = $attr->value;
    }
  }

  if ($dom->hasChildNodes()) {
    $children = $dom->childNodes;
    if ($children->length == 1) {
      $child = $children->item(0);
      if ($child->nodeType == XML_TEXT_NODE) {
        $result['_value'] = $child->nodeValue;
        return count($result) == 1 ? $result['_value'] : $result;
      }
    }
    $groups = array();
    foreach ($children as $child) {
      if (!isset($result[$child->nodeName])) {
        $result[$child->nodeName] = ($depth != 0) ? xml_to_array($child, $depth - 1) : FALSE;
      }
      else {
        if (!isset($groups[$child->nodeName])) {
          $result[$child->nodeName] = array($result[$child->nodeName]);
          $groups[$child->nodeName] = 1;
        }
        $result[$child->nodeName][] = ($depth != 0) ? xml_to_array($child, $depth - 1) : FALSE;
      }
    }
  }

  return $result;
}

/**
 * Object to an array conversion.
 *
 * @param mixed $var
 *
 * @return mixed
 */
function object_to_array($var) {
  if (!is_object($var) && !is_array($var)) {
    return $var;
  }
  return array_map('object_to_array', (array) $var);
}
