<?php

/**
 * @file
 * Debug functions.
 */

/**
 * @param $query
 * @param bool $return
 *
 * @return string
 */
function handy_dpq($query, $return = TRUE) {
  $sql = dpq($query, $return);
  if ($return) {
    return strtr($sql, array(
      '{' => '',
      '}' => '',
    ));
  }
}

/**
 * @param bool $return_string
 * @param string $separator
 * @param bool $arguments
 * @param bool $arguments_step
 *
 * @return array|string
 */
function handy_debug_backtrace($return_string = TRUE, $separator = "\n", $arguments = FALSE, $arguments_step = TRUE, $ob_clean = TRUE) {
  // Clears the previous output.
  if ($ob_clean) {
    ob_clean();
  }

  // Content-type.
  if ($return_string) {
    $mime = 'text/plain';
    if ($separator !== strip_tags($separator)) {
      $mime = 'text/html';
    }
    header('Content-type: ' . $mime);
  }

  $backtrace = array();
  $debug_backtrace = debug_backtrace();
  array_shift($debug_backtrace);
  foreach ($debug_backtrace as $index => $debug) {
    $debug += array(
      'file' => '%unknown%',
      'line' => '%unknown%',
      'function' => '%unknown%',
    );
    $backtrace[] = '[' . ($index + 1) . '] ' . $debug['function'] . ' in (line ' . $debug['line'] . ' of ' . $debug['file'] . ')';

    if ($arguments && !empty($debug['args'])) {
      if (($arguments_step === TRUE) || ($arguments_step == $index + 1)) {
        ob_start();
        print_r($debug['args']);
        $backtrace[] = ob_get_clean();
      }
    }
  }

  if ($return_string) {
    $backtrace = implode($separator, $backtrace);
  }

  return $backtrace;
}

/**
 * @param $text
 * @param string $prefix
 * @param bool $time
 */
function handy_cli($text, $prefix = '', $time = TRUE) {
  if ($f = fopen('php://stdout', 'w')) {
    if ($time) {
      $t = explode(' ', microtime());
      fwrite($f, '[' . date('Y-m-d H:i:s', $t[1]) . substr((string) $t[0], 1, 4) . '] ');
    }
    if ($prefix) {
      fwrite($f, '[' . $prefix . '] ');
    }
    fwrite($f, $text . "\n");
    fclose($f);
  }
}

/**
 * @param $message
 * @param $filepath
 * @param string $newline
 */
function handy_file_log($message, $filepath, $newline = "\n") {
  if (TRUE === $message) {
    unlink($filepath);
  }
  elseif ($file = fopen($filepath, 'a+')) {
    fwrite($file, $message . $newline);
    fclose($file);
  }
}

/**
 * @param int $index
 *
 * @return bool
 */
function handy_get_callee($index = 2) {
  $backtrace = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);
  return isset($backtrace[$index]['function']) ? $backtrace[$index]['function'] : FALSE;
}

/**
 * @param null $message
 * @param int $level
 */
function handy_profiler($message = NULL, $level = 0) {
  static $messages = array(), $mtimes = array(), $childs = array();

  $mtime = microtime(TRUE);

  // default mtime on init
  if (is_null($message) && (0 === $level)) {
    $mtimes[0] = $mtime;
    return;
  }

  // print
  if (TRUE === $message) {
    _handy_profiler_print($messages);
    return;
  }

  // propagate the mtime
  if (!isset($mtimes[$level])) {
    if (isset($mtimes[$level - 1])) {
      $mtimes[$level] = $mtimes[$level - 1];
    }
    else {
      $mtimes[$level] = $mtime;
    }
  }
  $mtimes[$level + 1] = $mtime;

  // save message
  $id = uniqid($level . '_');
  $messages[$id] = array(
    'mtime' => round($mtime - $mtimes[$level], 4),
    'text' => $message,
    'childs' => array(),
  );
  $mtimes[$level] = $mtime;

  // childs
  if (!empty($childs[$level + 1])) {
    $messages[$id]['childs'] = $childs[$level + 1];
    $childs[$level + 1] = array();
  }

  // save childs
  $childs[$level][] = $id;
}

/**
 * @param       $messages
 * @param array $ids
 */
function _handy_profiler_print(&$messages, $ids = array()) {
  if (empty($ids)) {
    foreach ($messages as $id => $message) {
      if (0 === strpos($id, '0')) {
        $ids[] = $id;
      }
    }
  }

  echo '<ul>';
  foreach ($ids as $id) {
    $message = $messages[$id];
    echo '<li>[', $message['mtime'], '] ', $message['text'];
    if (!empty($message['childs'])) {
      _handy_profiler_print($messages, $message['childs']);
    }
    echo '</li>';
  }
  echo '</ul>';
}
